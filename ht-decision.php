<!DOCTYPE html>
<html lang="ja">
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <meta name="viewport" content="width=device-width">
  <title>ハッピーターン判定デモ</title>
  <!-- Latest compiled and minified jquery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <style type="text/css">
    body { padding-bottom: 200px; }
  </style>
</head>
  <nav class="navbar navbar-default navbar-fixed-bottom">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#gnavi">
          <span class="sr-only">メニュー</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div id="gnavi" class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
         <p class="navbar-text"><a href="/index.html">トップページへ戻る</a></p>
        </ul>
      </div>
    </div>
  </nav>
<body>
  <div class="container">
    <h3>ハッピーターン判定結果</h3>
    <h4>■結果</h4>
    <?php
      // セッション開始
      session_cache_limiter('private_no_expire');
      @session_start();
      // 外部関数呼び出し
      require_once __DIR__.'/function.php';
      // 言語設定とエラー出力設定
      setlocale(LC_ALL, 'ja_JP.UTF-8');
      //ini_set('display_errors', 1);
      // 変数宣言
      $upload= "files/".date("YmdHis_")."before.jpg";
      $output = "files/".date("YmdHis_")."after.jpg";
      $map = "files/map.jpg";
      $replace_w = array("_before","_after");

      // main処理
      //// ファイルアップロード処理
      if(is_uploaded_file($_FILES["files"]["tmp_name"][0])) {
        if(move_uploaded_file($_FILES["files"]["tmp_name"][0],$upload)) {
          //echo "<p>". $_FILES["files"]["name"][0] . "をアップロードしました。</p><br>";
        } else {
          echo "アップロードエラー<br>";
        }
      }
      //// 回転処理
      orientationFixedImage($output,$upload);

      //// ハッピーターン認識
      $result = VR_POST($output);

      //// 結果表示
      $Table1 = "<table class=\"table table-bordered table-sm\">";
      $Table1 .= "<tr><th calss=\"col-sm-2\">Classes</th>"
                 ."<th calss=\"col-sm-2\">Score</th><th calss=\"col-sm-3\">Watsonさんのコメント</th></tr>";
      if(!empty($result["images"][0]["classifiers"][0]["classes"][0]["score"])){
        $Score = $result["images"][0]["classifiers"][0]["classes"][0]["score"]*100;
      }else{
        $Score = 0;
      }
      if($Score >= 50){
        if($result["images"][0]["classifiers"][0]["classes"][0]["class"] == "happyturn"){
          $Table1 .= "<tr class=\"success\"><td>ハッピーターン</td><td>{$Score}%</td>"
                     ."<td>コレハ「ハッピーターン」デスネ。「ハッピーターン」サイコウ。タベタイ</td></tr>\n";
        }else if($result["images"][0]["classifiers"][0]["classes"][0]["class"] == "bakauke")
          $Table1 .= "<tr class=\"success\"><td>ばかうけ</td><td>{$Score}%</td>"
                    ."<td>コレハ「ばかうけ」デスネ。「ばかうけ」サイコウ。タベタイ</td></tr>\n";
        }else{
          $Table1 .= "<tr class=\"warning\"><td>なぞのぶったい</td><td>{$Score}%</td>"
                     ."<td>ニンシキデキナイブッタイデス。モウシワケゴザイマセン</td></tr>\n";
      }
      $Table1 .= "</table>";
      echo "{$Table1}";
      $Image = "<img src=\"$output\" alt=\"result\" width=\"200\" height=\"300\"><br>";
      echo "{$Image}";
    ?>
    <h4>■生Json</h4>
    <?php
      //// JSON結果の出力
      echo "<pre>";
      print_r(json_encode($result,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      echo "</pre>";
      //// 古いアップロード画像の削除
      $delet_time = date('YmdHis',strtotime( "-5 min" ));
      $dir = opendir("files/");
      while (false !== ($file = readdir($dir))){
        if($file[0] != "."){
          $delet_file = "files/".$file;
          $target = basename($file, ".jpg");
          $delet_file_time = str_replace($replace_w,'',$target);
          if($delet_time > $delet_file_time) {
            unlink($delet_file);
          }
        }
      }
      closedir($dir);
    ?>
  </div>
</body>
</html>