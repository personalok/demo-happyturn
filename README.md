# プログラム概要
* index.html  
 * トップページ。アップロードする画像のフォームとプレビュー機能を提供しています。  
* ht-decision.php  
 * VisualRecognitionの判定結果を表示するためのページです。ページ内で、画像の回転処理、VisualRecognitionへのPOST、結果表示処理を行っています
* function.php
 * ht-decision.phpで呼びだされている関数を記載したプログラムです

# 使い方
* Bluemix上でVisualRecognitionの環境を構築  
* VisualRecognitionに学習データをPOSTし、Watsonに学習させて下さい。実行結果から得られる「classifier_ids」はメモして下さい  
* 「functions.php」の94行目(「api_key」と「日付」)と97行目(「classifier id」)を修正して下さい  
* 「manifest.yml」の「name」・「host」を修正して下さい
* 「cf push」で全てのファイルをBluemixにソースコードをアップロードします
