<?php
//使用例/////////////////////////////////
//orientationFixedImage($output,$input);
////////////////////////////////////////
// 画像の左右反転
function image_flop($image){
  // 画像の幅を取得
	$w = imagesx($image);
  // 画像の高さを取得
	$h = imagesy($image);
  // 変換後の画像の生成（元の画像と同じサイズ）
	$destImage = @imagecreatetruecolor($w,$h);
  // 逆側から色を取得
	for($i=($w-1);$i>=0;$i--){
		for($j=0;$j<$h;$j++){
			$color_index = imagecolorat($image,$i,$j);
			$colors = imagecolorsforindex($image,$color_index);
			imagesetpixel($destImage,abs($i-$w+1),$j,imagecolorallocate($destImage,$colors["red"],$colors["green"],$colors["blue"]));
		}
	}
	return $destImage;
}

// 上下反転
function image_flip($image){
  // 画像の幅を取得
	$w = imagesx($image);
  // 画像の高さを取得
	$h = imagesy($image);
  // 変換後の画像の生成（元の画像と同じサイズ）
	$destImage = @imagecreatetruecolor($w,$h);
  // 逆側から色を取得
	for($i=0;$i<$w;$i++){
		for($j=($h-1);$j>=0;$j--){
			$color_index = imagecolorat($image,$i,$j);
			$colors = imagecolorsforindex($image,$color_index);
			imagesetpixel($destImage,$i,abs($j-$h+1),imagecolorallocate($destImage,$colors["red"],$colors["green"],$colors["blue"]));
		}
	}
	return $destImage;
}

// 画像を回転
function image_rotate($image, $angle, $bgd_color){
	return imagerotate($image, $angle, $bgd_color, 0);
}

// 画像の方向を正す
function orientationFixedImage($output,$input){
  $re = "";
	$image = imagecreatefromjpeg($input);
	$exif_datas = exif_read_data($input);
	if(isset($exif_datas['Orientation'])){
		$orientation = $exif_datas['Orientation'];
		if($image){
			if($orientation == 0){// 未定義
			}else if($orientation == 1){// 通常
				imagejpeg($image ,$output ,70);
			}else if($orientation == 2){// 左右反転
				$re = image_flop($image);
                imagejpeg($re ,$output ,70);
			}else if($orientation == 3){// 180°回転
				$re = image_rotate($image,180, 0);
                imagejpeg($re ,$output ,70);
			}else if($orientation == 4){// 上下反転
				$re = image_Flip($image);
                imagejpeg($re ,$output ,70);
			}else if($orientation == 5){// 反時計回りに90°回転 上下反転
				$rotate = image_rotate($image,90, 0);
				$re = image_flip($rotate);
                imagejpeg($re ,$output ,70);
			}else if($orientation == 6){// 時計回りに90°回転
				$re = image_rotate($image,270, 0);
                imagejpeg($re ,$output ,70);
			}else if($orientation == 7){// 時計回りに90°回転 上下反転
				$rotate = image_rotate($image,270, 0);
				$re = image_flip($rotate);
                imagejpeg($re ,$output ,70);
			}else if($orientation == 8){// 反時計回りに90°回転
				$re = image_rotate($image,90, 0);
                imagejpeg($re ,$output ,70);
			}
		}
	}else{
    imagejpeg($image ,$output ,70);
	}
}

// ハッピーターン認識処理(Visual RecognitionのPOST)
function VR_Post($jpg){
  try {
    #変数宣言
    $url = 'https://gateway-a.watsonplatform.net/visual-recognition/api/v3/classify'
          .'?api_key=<Input your key>&version=<Input your version>';
    $curl = curl_init();
    $data = array("images_file" => new CURLFile($jpg,mime_content_type($jpg),basename($jpg)),
    	            "classifier_ids" => "<Input your id>");
    curl_setopt($curl, CURLOPT_URL, $url);
    //curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
    curl_setopt($curl, CURLOPT_POST, TRUE);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $vr_exec = curl_exec($curl);
    curl_close($curl);
    $re = json_decode($vr_exec,true);
    return $re;
  } catch(Exception $e){
    echo $e->getMessage();
  }
}

?>